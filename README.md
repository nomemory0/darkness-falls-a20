# Darkness Falls - A20 Edition

Darkness Falls is an overhaul mod for 7 Days to Die by The Fun Pimps. This particular repository contains all the files needed to play the mod in single player or multiplayer. It is only compatible with Alpha 20.

## Installation Guide

1) Click the download button next to "Clone" and select "Download as Zip".
2) The zip file you downloaded should be called "darkness-falls-a20-main.zip". Open it in your favourite unzipping program (7-Zip recommended as it's good and free!)
3) Inside the zip file, you should find 2 files (an icon file and serverconfig.xml) and a single folder called Mods. This folder contains ALL the files the mod needs.
4) Open steam, right-click 7 Days to Die and select properties.
5) In the new window, select "Local Files" and then "Browse". This will open your 7 days to die directory.
6) If you don't want to run vanilla AND the mod, drag all the files from the zip you opened (and should've kept open) into the window where 7 Days to Die is.
7) In the 7 Days to Die directory, you will see 2 EXE files. 7DaystoDie.exe and 7DaystoDie_EAC.exe. Run 7DaystoDie.exe as Darkness Falls is NOT compatible with EAC.

Enjoy the mod!

## Links
1) Discord: https://discord.gg/m3UGP4hpdw
2) Khaine's Twitter: https://twitter.com/KhainesKorner 
3) Khaine's Youtube (Where he gets destroyed by his own mod): https://www.youtube.com/c/KhainesKorner 
4) Darkness Falls Forum Post: https://community.7daystodie.com/topic/4941-darkness-falls-they-mostly-come-out-at-night/ 
5) Nexus Mods Link: https://www.nexusmods.com/7daystodie/mods/235?tab=description&BH=0 
6) Patreon (For folks who wish to contribute on a monthly basis): https://www.patreon.com/darknessfallsmod 
7) Paypal (For folks who wish to contribute as a one-off): https://www.paypal.me/khainegb 
